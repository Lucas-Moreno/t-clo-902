import jwt, { Secret } from "jsonwebtoken";
import process from "process";
import { NextFunction } from "express";
import UserModel from "../models/user.models";
import { Request, Response } from "../types/types";
interface JwtPayload {
  id: string;
}

export const authMiddleware = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let token: string = "";

    const cookiesHeader = req.headers["cookie"];

    let jwtCookieValue;

    if (cookiesHeader) {
      const cookies = cookiesHeader.split("; ");
      for (const cookie of cookies) {
        if (cookie.startsWith("jwt=")) {
          jwtCookieValue = cookie.substring(4);
          break;
        }
      }
    }

    if (jwtCookieValue) {
      token = jwtCookieValue;
    }

    if (token === "") {
      return res.status(401).json({ message: "Unauthorized" });
    }

    const secret: Secret = process.env.JWT_SECRET!;

    const decodedToken = jwt.verify(token, secret) as JwtPayload;

    const user = await UserModel.findById(decodedToken.id);

    if (!user) {
      return res.status(401).json({ message: "Unauthorized" });
    }

    req.user = user;

    next();
  } catch (error) {
    res.status(401).json({ message: "Unauthorized" });
  }
};
